<?php

namespace yii2portal\media\models;

use Imagine\Exception\InvalidArgumentException;
use Imagine\Image\Box;
use Yii;
use yii\imagine\Image;

/**
 * This is the model class for table "user_upload".
 *
 * @property integer $id
 * @property integer $uid
 * @property string $file_type
 * @property integer $dateline
 * @property integer $file_size
 * @property string $filename
 * @property string $ext
 * @property string $ip
 * @property string $groupid
 * @property string $title
 * @property string $description
 * @property string $source
 * @property integer $public
 * @property integer $views
 * @property integer $clicks
 * @property string $content_type
 * @property string $width
 * @property string $height
 * @property string $params
 * @property string $module
 * @property string $module_item
 */
class Media extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_upload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'dateline', 'file_size', 'public', 'views', 'clicks'], 'integer'],
            [['description', 'params', 'module', 'module_item'], 'string'],
            [['file_type', 'filename', 'groupid', 'title', 'source'], 'string', 'max' => 255],
            [['ext', 'width', 'height'], 'string', 'max' => 5],
            [['ip'], 'string', 'max' => 20],
            [['content_type'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'file_type' => 'File Type',
            'dateline' => 'Dateline',
            'file_size' => 'File Size',
            'filename' => 'Filename',
            'ext' => 'Ext',
            'ip' => 'Ip',
            'groupid' => 'Groupid',
            'title' => 'Title',
            'description' => 'Description',
            'source' => 'Source',
            'public' => 'Public',
            'views' => 'Views',
            'clicks' => 'Clicks',
            'content_type' => 'Content Type',
            'width' => 'Width',
            'height' => 'Height',
            'module' => 'Module',
            'module_item' => 'Module Item',
        ];
    }

    public function getSrcUrl()
    {
//        if ($this->file_type == 'image') {
            $imagePath = Yii::getAlias(Yii::$app->params['media']['src']);
            return "{$imagePath}/" . floor($this->id / 1000) . "/{$this->id}.{$this->ext}";
//        }
    }

    public function thumbnail($width, $height, $default = null)
    {
        if ($this->file_type == 'image') {
            $imagesPath = realpath(Yii::getAlias(Yii::$app->params['media']['path']));

            $filePath = "{$imagesPath}/" . floor($this->id / 1000) . "/{$this->id}.{$this->ext}";

            $pathHash = md5($filePath);

            $fst = substr($pathHash, 0, 5);
            $sec = substr($pathHash, 5, 5);

            $imageLoc = "{$fst}/{$sec}";

            $dirPath = Yii::getAlias(Yii::$app->params['media']['thumbnail']['path'] . "/{$imageLoc}");

            if (!file_exists($dirPath)) {
                mkdir($dirPath, 0777, true);
            }

            $thumbSrc = "{$this->id}_w{$width}_h{$height}.{$this->ext}";
            $thumbPath = "{$dirPath}/{$thumbSrc}";

            if (!file_exists($thumbPath)) {
                try {
                    if(file_exists($filePath)) {
                        list($originalWidth, $originalHeight) = getimagesize($filePath);
                        if (!$width && $height) {
                            $width = $originalWidth / $originalHeight * $height;
                        } elseif ($width && !$height) {
                            $height = $originalWidth / $originalHeight * $width;
                        }


                        if ($originalWidth > $width) {
                            Image::thumbnail($filePath, $width, $height)
                                ->save("{$thumbPath}", ['quality' => 80]);
                        } else {
                            $imagine = Image::getImagine();
                            $img = $imagine->open($filePath);
                            $thumbnailBox = new Box($width, $height);
                            $img->resize($thumbnailBox)->save("{$thumbPath}", ['quality' => 80]);
                        }
                    }else{
                        return $default;
                    }
                } catch (InvalidArgumentException $e) {
                    Yii::error($e->getMessage());
                    return $default;
                }
            }

            return Yii::$app->params['media']['thumbnail']['url'] . "/{$imageLoc}/{$thumbSrc}";
        }
        return null;
    }

    public function resize($width, $height, $default = null, $additional_key=null)
    {
        if ($this->file_type == 'image') {
            $imagesPath = realpath(Yii::getAlias(Yii::$app->params['media']['path']));

            $filePath = "{$imagesPath}/" . floor($this->id / 1000) . "/{$this->id}.{$this->ext}";

            $pathHash = md5($filePath);

            $fst = substr($pathHash, 0, 5);
            $sec = substr($pathHash, 5, 5);

            $imageLoc = "{$fst}/{$sec}";

            $dirPath = Yii::getAlias(Yii::$app->params['media']['thumbnail']['path'] . "/{$imageLoc}");

            if (!file_exists($dirPath)) {
                mkdir($dirPath, 0777, true);
            }

            if($additional_key){
                $thumbSrc = "{$this->id}_w{$width}_h{$height}_{$additional_key}_r.{$this->ext}";
            }else{
                $thumbSrc = "{$this->id}_w{$width}_h{$height}_r.{$this->ext}";
            }

            $thumbPath = "{$dirPath}/{$thumbSrc}";

            if (!file_exists($thumbPath)) {
                try {


                    Image::thumbnail($filePath, $width, $height)
                        ->save("{$thumbPath}", ['quality' => 80]);
                } catch (InvalidArgumentException $e) {
                    Yii::error($e->getMessage());
                    return $default;
                }
            }

            return Yii::$app->params['media']['thumbnail']['url'] . "/{$imageLoc}/{$thumbSrc}";
        }
        return null;
    }

    public function crop($width, $height, $default = null, $additional_key=null)
    {
        if ($this->file_type == 'image') {
            $imagesPath = realpath(Yii::getAlias(Yii::$app->params['media']['path']));

            $filePath = "{$imagesPath}/" . floor($this->id / 1000) . "/{$this->id}.{$this->ext}";

            $pathHash = md5($filePath);

            $fst = substr($pathHash, 0, 5);
            $sec = substr($pathHash, 5, 5);

            $imageLoc = "{$fst}/{$sec}";

            $dirPath = Yii::getAlias(Yii::$app->params['media']['thumbnail']['path'] . "/{$imageLoc}");

            if (!file_exists($dirPath)) {
                mkdir($dirPath, 0777, true);
            }

            if($additional_key){
                $thumbSrc = "{$this->id}_w{$width}_h{$height}_{$additional_key}_c.{$this->ext}";
            }else{
                $thumbSrc = "{$this->id}_w{$width}_h{$height}_c.{$this->ext}";
            }
            $thumbPath = "{$dirPath}/{$thumbSrc}";

            if (!file_exists($thumbPath)) {
                try {
                    if (!$width && $height) {
                        list($originalWidth, $originalHeight) = getimagesize($filePath);
                        $width = $originalWidth / $originalHeight * $height;
                    } elseif ($width && !$height) {
                        list($originalWidth, $originalHeight) = getimagesize($filePath);
                        $height = $originalWidth / $originalHeight * $width;
                    }


                    Image::crop($filePath, $width, $height)
                        ->save("{$thumbPath}", ['quality' => 80]);
                } catch (InvalidArgumentException $e) {
                    Yii::error($e->getMessage());
                    return $default;
                }
            }

            return Yii::$app->params['media']['thumbnail']['url'] . "/{$imageLoc}/{$thumbSrc}";
        }
        return null;
    }


    public function getDescription()
    {
        $params = unserialize($this->params);
        return isset($params['descr']) ? $params['descr'] : null;
    }

    public function getSource()
    {
        $params = unserialize($this->params);
        return isset($params['source']) ? $params['source'] : null;
    }

    public function getSourceUrl()
    {
        $params = unserialize($this->params);
        return isset($params['source_url']) ? $params['source_url'] : null;
    }

    /**
     * @inheritdoc
     * @return MediaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MediaQuery(get_called_class());
    }
}
