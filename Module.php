<?php

namespace yii2portal\media;

use Yii;
use yii2portal\media\components\cplugin\Gallery;
use yii2portal\media\components\cplugin\Photo;

class Module extends \yii2portal\core\Module
{
    public function init()
    {
        parent::init();

        Yii::$app->getModule('cplugin')->registerPlugin(Gallery::className());
        Yii::$app->getModule('cplugin')->registerPlugin(Photo::className());
    }

}