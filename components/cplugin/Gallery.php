<?php

namespace yii2portal\media\components\cplugin;

use yii2portal\cplugin\models\Cplugin;
use yii2portal\media\models\Media;


class Gallery extends Cplugin
{

    public function pluginChk($params)
    {
        return array(
            'status' => true,
            'params' => $params
        );
    }

    public function pluginConfig($params)
    {

        return array(
            'html' => $this->render('config', array('params' => $params)),
            'config' => true,
            'resizable' => false,
            'styles' => false
        );
    }

    public function pluginRender($params)
    {
        $return = '';

        if(isset($params['ids'])) {
            $ids = explode(',', $params['ids']);
            $photos = Media::find()
                ->where([
                    'id' => $ids
                ])->andWhere([
                    'file_type' => 'image'
                ])
                ->indexBy('id')
                ->all();

            if (!empty($photos)) {
                $sorted = [];
                foreach ($ids as $id) {
                    if (isset($photos[$id])) {
                        $sorted[] = $photos[$id];
                    }
                }
                $params['width'] = isset($params['width'])?$this->getSize($params['width']):'0';
                $return = $this->render('gallery', array('photos' => $sorted, 'params' => $params));
            }
        }
        return $return;
    }
}