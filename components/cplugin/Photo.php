<?php

namespace yii2portal\media\components\cplugin;

use yii2portal\cplugin\models\Cplugin;
use yii2portal\media\models\Media;


class Photo extends Cplugin
{

    public function pluginChk($params)
    {
        return array(
            'status' => true,
            'params' => $params
        );
    }

    public function pluginConfig($params)
    {

        return array(
            'html' => $this->render('config', array('params' => $params)),
            'config' => true,
            'resizable' => false,
            'styles' => false
        );
    }

    public function pluginRender($params)
    {
        $return = '';
        if(isset($params['id'])) {
            $id = $params['id'];
            $photo = Media::findOne($id);
            if ($photo) {
                $params['width'] = isset($params['width'])?$this->getSize($params['width']):'0';
                $return = $this->render('photo', array('photo' => $photo, 'params' => $params));
            }
        }
        return $return;
    }
}